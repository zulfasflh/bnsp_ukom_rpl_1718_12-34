<?php

namespace App;

use App\Helpers\Check;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Barang;

class Barang extends Model
{
    protected $table = 'barang';
    protected $fillable = [
        'id_barang',
        'kd_barang',
        'nama_petugas',
        'nama', 
        'nama_ruang',
        'nama_jenis',
        'kondisi', 
        'keterangan', 
        'jumlah', 
        'gambar'
    ];

    // set koneksi hak akses db
    public function __construct(array $attributes = [])
    {
        $this->bootIfNotBooted();
        $this->fill($attributes);
        $this->setConnection(Check::connection());
    }

    //fungsi untuk relasi ke tabel lain
    public function petugas()
    {
        return $this->belongsTo(Petugas::class);
    }

    public function jenis()
    {
        return $this->belongsTo(JenisBrg::class);
    }

    public function ruang()
    {
        return $this->belongsTo(Ruang::class);
    }

    public function peminjaman()
    {
        return $this->belongsTo(Peminjaman::class);
    }

    public function getGambar()
    {
        if (!$this->gambar) {
            return asset('images/default2.gif');
        }
        return asset('images/brg/'.$this->gambar);
    }

}
