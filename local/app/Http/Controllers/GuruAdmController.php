<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\Guru;

class GuruAdmController extends Controller
{
    public function index()
    {
        return view('admin.guru.index');
    }

    public function add()
    {
        return view('admin.guru.tambah');
    }

    public function create(Request $request)
    {
        $user = new User;
        $user->username = $request->username;
        $user->password = $request->password;
        $user->id_level = "2";
        $user->save();

        $user->$request->request = ['guru' => 'id_user'];
        $x = Guru::create();

        // if(!empty hasFile('gambar'){
        //     if(hasFile('gambar')->move());
        // });
        return redirect('admin.guru.index', compact('user', 'x'));
    }

    public function cari(Request $request)
    {
        $cari = $request->cari;
        $x = Guru::all()->where('nama', 'like','%'.$cari.'%');
        return redirect('admin.guru.index', compact('cari','x'));
    }
}
