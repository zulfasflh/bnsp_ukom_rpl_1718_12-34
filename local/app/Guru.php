<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Guru extends Model
{
    protected $table = 'guru';
    protected $fillable = [
        'id_mapel',
        'id_user',
        'nip',
        'nama',
        'alamat',
        'telpon',
        'gambar'
    ];

    public function user()
    {
        return $this->hasOne('App\User');
    }

    
}
