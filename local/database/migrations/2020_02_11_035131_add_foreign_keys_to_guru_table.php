<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToGuruTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('guru', function(Blueprint $table)
		{
			$table->foreign('id_mapel', 'id_mapel_fk_1')->references('id')->on('mapel')->onUpdate('CASCADE')->onDelete('CASCADE');
			$table->foreign('id_user', 'id_user_fk_1')->references('id')->on('users')->onUpdate('CASCADE')->onDelete('CASCADE');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('guru', function(Blueprint $table)
		{
			$table->dropForeign('id_mapel_fk_1');
			$table->dropForeign('id_user_fk_1');
		});
	}

}
