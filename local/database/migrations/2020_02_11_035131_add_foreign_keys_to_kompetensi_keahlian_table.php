<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToKompetensiKeahlianTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('kompetensi_keahlian', function(Blueprint $table)
		{
			$table->foreign('id_bidang', 'id_bidang_fk_1')->references('id')->on('bidang_studi')->onUpdate('CASCADE')->onDelete('CASCADE');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('kompetensi_keahlian', function(Blueprint $table)
		{
			$table->dropForeign('id_bidang_fk_1');
		});
	}

}
