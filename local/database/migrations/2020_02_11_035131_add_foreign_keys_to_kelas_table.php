<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToKelasTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('kelas', function(Blueprint $table)
		{
			$table->foreign('id_kompetensi', 'id_kompetensi_fk_1')->references('id')->on('kompetensi_keahlian')->onUpdate('CASCADE')->onDelete('CASCADE');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('kelas', function(Blueprint $table)
		{
			$table->dropForeign('id_kompetensi_fk_1');
		});
	}

}
