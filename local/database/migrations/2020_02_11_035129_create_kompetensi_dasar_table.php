<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateKompetensiDasarTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('kompetensi_dasar', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->integer('id_mapel')->index('id_mapel_fk_2');
			$table->integer('id_kelas')->index('id_kelas_fk_1');
			$table->string('nama', 50);
			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('kompetensi_dasar');
	}

}
