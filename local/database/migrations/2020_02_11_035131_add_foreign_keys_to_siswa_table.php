<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToSiswaTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('siswa', function(Blueprint $table)
		{
			$table->foreign('id_kelas', 'id_kelas_fk_3')->references('id')->on('kelas')->onUpdate('CASCADE')->onDelete('CASCADE');
			$table->foreign('id_kompetensi', 'id_kompetensi_fk_2')->references('id')->on('kompetensi_keahlian')->onUpdate('CASCADE')->onDelete('CASCADE');
			$table->foreign('id_user', 'id_user_fk_2')->references('id')->on('users')->onUpdate('CASCADE')->onDelete('CASCADE');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('siswa', function(Blueprint $table)
		{
			$table->dropForeign('id_kelas_fk_3');
			$table->dropForeign('id_kompetensi_fk_2');
			$table->dropForeign('id_user_fk_2');
		});
	}

}
