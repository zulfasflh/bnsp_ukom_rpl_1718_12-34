<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToNilaiTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('nilai', function(Blueprint $table)
		{
			$table->foreign('id_guru', 'id_guru_fk_1')->references('id')->on('guru')->onUpdate('CASCADE')->onDelete('CASCADE');
			$table->foreign('id_jns_nilai', 'id_jenis_nilai_fk_1')->references('id')->on('jenis_nilai')->onUpdate('CASCADE')->onDelete('CASCADE');
			$table->foreign('id_kd', 'id_kd_fk_1')->references('id')->on('kompetensi_dasar')->onUpdate('CASCADE')->onDelete('RESTRICT');
			$table->foreign('id_kelas', 'id_kelas_fk_2')->references('id')->on('kelas')->onUpdate('CASCADE')->onDelete('CASCADE');
			$table->foreign('id_mapel', 'id_mapel_fk_3')->references('id')->on('mapel')->onUpdate('CASCADE')->onDelete('CASCADE');
			$table->foreign('id_siswa', 'id_siswa_fk_1')->references('id')->on('siswa')->onUpdate('CASCADE')->onDelete('CASCADE');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('nilai', function(Blueprint $table)
		{
			$table->dropForeign('id_guru_fk_1');
			$table->dropForeign('id_jenis_nilai_fk_1');
			$table->dropForeign('id_kd_fk_1');
			$table->dropForeign('id_kelas_fk_2');
			$table->dropForeign('id_mapel_fk_3');
			$table->dropForeign('id_siswa_fk_1');
		});
	}

}
