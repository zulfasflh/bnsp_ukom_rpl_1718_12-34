<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateGuruTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('guru', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->integer('id_mapel')->index('id_mapel_fk_1');
			$table->bigInteger('id_user')->unsigned()->index('id_user_fk_1');
			$table->char('nip', 20);
			$table->string('nama', 30);
			$table->text('alamat', 65535);
			$table->bigInteger('telpon');
			$table->string('gambar');
			$table->timestamps();

		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('guru');
	}

}
