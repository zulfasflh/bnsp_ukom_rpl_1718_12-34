<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToWaliMuridTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('wali_murid', function(Blueprint $table)
		{
			$table->foreign('id_siswa', 'id_siswa_fk_2')->references('id')->on('siswa')->onUpdate('CASCADE')->onDelete('CASCADE');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('wali_murid', function(Blueprint $table)
		{
			$table->dropForeign('id_siswa_fk_2');
		});
	}

}
