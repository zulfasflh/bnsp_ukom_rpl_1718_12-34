<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateSiswaTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('siswa', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->integer('id_kompetensi')->index('id_kompetensi_fk_2');
			$table->integer('id_kelas')->index('id_kelas_fk_3');
			$table->bigInteger('id_user')->unsigned()->index('id_user_fk_2');
			$table->integer('nisn');
			$table->string('nama', 50);
			$table->text('alamat', 65535);
			$table->date('tgl_lahir');
			$table->string('gambar');
			$table->timestamps();

		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('siswa');
	}

}
