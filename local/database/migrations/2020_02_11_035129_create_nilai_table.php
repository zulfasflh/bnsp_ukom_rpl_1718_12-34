<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateNilaiTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('nilai', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->integer('id_siswa')->index('id_siswa_fk_1');
			$table->integer('id_guru')->index('id_guru_fk_1');
			$table->integer('id_mapel')->index('id_mapel_fk_3');
			$table->integer('id_kd')->index('id_kd_fk_1');
			$table->integer('id_kelas')->index('id_kelas_fk_2');
			$table->integer('id_jns_nilai')->index('id_jenis_nilai_fk_1');
			$table->integer('semester');
			$table->float('nilai_akhir', 10, 0);
			$table->char('akreditasi', 10);
			$table->timestamps();

		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('nilai');
	}

}
