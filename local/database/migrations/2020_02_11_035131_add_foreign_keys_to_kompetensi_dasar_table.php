<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToKompetensiDasarTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('kompetensi_dasar', function(Blueprint $table)
		{
			$table->foreign('id_kelas', 'id_kelas_fk_1')->references('id')->on('kelas')->onUpdate('CASCADE')->onDelete('CASCADE');
			$table->foreign('id_mapel', 'id_mapel_fk_2')->references('id')->on('mapel')->onUpdate('CASCADE')->onDelete('CASCADE');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('kompetensi_dasar', function(Blueprint $table)
		{
			$table->dropForeign('id_kelas_fk_1');
			$table->dropForeign('id_mapel_fk_2');
		});
	}

}
