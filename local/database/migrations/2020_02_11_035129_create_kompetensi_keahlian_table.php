<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateKompetensiKeahlianTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('kompetensi_keahlian', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->integer('id_bidang')->index('id_bidang_fk_1');
			$table->string('nama', 50);
			$table->timestamps();

		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('kompetensi_keahlian');
	}

}
