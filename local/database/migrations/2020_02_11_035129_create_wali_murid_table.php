<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateWaliMuridTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('wali_murid', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->integer('id_siswa')->index('id_siswa_fk_2');
			$table->string('nm_ayah', 30);
			$table->string('pek_ayah', 30);
			$table->string('nm_ibu', 30);
			$table->string('pek_ibu', 30);
			$table->bigInteger('telpon');
			$table->timestamps();

		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('wali_murid');
	}

}
