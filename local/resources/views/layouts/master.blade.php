<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Score's</title>
    <link rel="stylesheet" href="{{url('bootstrap/css/bootstrap.min.css')}}">
    <link rel="stylesheet" href="{{url('css/score.css')}}">

    <script defer src="{{asset('fontawesome/js/solid.js')}}"></script>
    <script defer src="{{asset('fontawesome/js/fontawesome.js')}}"></script>
</head>
<body>

    <!-- WRAPPER CONTENT -->
    <div class="wrapper">
        <!-- SIDEBAR -->
        <nav id="sidebar">
            <div class="sidebar-header">
            <img src="{{asset('img/logo.png')}}" class="logo">
                <h5 class="display-5 title-side rounded text-center">Username</h5>
            </div>

            <ul class="list-unstyled components">
                <li>
                <a href="{{url('dashboard')}}"><i class="fas fa-home" style="color:#444"></i> Beranda</a>
                </li>
                <li>
                <a href="{{url('siswa')}}"><i class="fas fa-user-graduate" style="color:#444"></i> Siswa</a>
                </li>
                <li>
                <a href="{{url('guru')}}"><i class="fas fa-user-tie" style="color:#444"></i> Guru</a>
                </li>
                <li>
                <a href="{{url('mapel')}}"><i class="fas fa-book" style="color:#444"></i> Mapel</a>
                </li>
                <li>
                <a href="{{url('bidang')}}"><i class="fas fa-book-open" style="color:#444"></i> Bidang Studi</a>
                </li>
                <li>
                <a href="{{url('kompetensi')}}"><i class="fas fa-chalkboard-teacher" style="color:#444"></i> Kompetensi Keahlian</a>
                </li>
                <li>
                <a href="{{url('kd')}}"><i class="fas fa-chalkboard-teacher" style="color:#444"></i> Kompetensi Dasar</a>
                </li>
                <li>
                <a href="#pageSubmenu" aria-expanded="false" class="dropdown-toggle" data-toggle="collapse"><i class="fas fa-star" style="color:#444"></i> Nilai</a>
                <ul class="list-unstyled components" id="pageSubmenu">
                <li>
                    <a href="{{url('kelolanilai')}}">Kelola Nilai</a>
                </li>
                <li>
                    <a href="{{url('lihatnilai')}}">Lihat Nilai</a>
                </li>
                </ul>
                </li>
            </ul>



        </nav>
        
        <!-- END SIDEBAR -->

        <div id="content">
            <nav class="navbar navbar-collapse navbar-dark rounded" style="background:#506884">
                <button class="navbar-toggler"><i class="fas fa-align-justify"></i></button>
                <a href="" id="sidebarCollapse">
                <button type="button" aria-controls="navbarNav" data-toggle="collapse" data-target="#navbarNav" aria-expanded="false" aria-label="Toggle navigation"><i class="fas fa-align-justify"></i></button>
                </a>

                <div class="collapse navbar-collapse">
                <ul class="list-unstyled components">
                    <li>
                        <a href="">Keluar</a>
                    </li>
                </ul>
                </div>
            </nav>

            @yield('content')
        </div>

    </div>
    <!-- END WRAPPER -->


<script defer src="{{asset('bootstrap/js/bootstrap.min.js')}}"></script>
<script defer src="{{asset('js/jquery.slim.min.js')}}"></script>
<script defer src="{{asset('js/jquery.min.js')}}"></script>
<script defer src="{{asset('js/popper.min.js')}}"></script>
<script defer src="{{asset('js/popper.js')}}"></script>
<script defer src="{{asset('js/highcharts.js')}}"></script>
<script>
$(document).ready(function(){
    $('#sidebarCollapse').on('click', function(){
        $('#sidebar').toggleClass('active');
    });
});
</script>
</body>
</html>