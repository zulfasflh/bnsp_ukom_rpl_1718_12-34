@extends('layouts.master')
@section('content')

<div class="container">
    <div class="jumbotron" style="background:transparent;color:#444;margin-top:-40px;">
        <h5 style="font-size:2.5em">
            DATA GURU
        </h4>
        <hr>
    </div>
</div>

<div class="container">
<form action="{{route('guru/cari')}}" method="get" name="cari">
<input type="text" placeholder="search" class="col-md-2 form-control float-left" value="{{old('cari')}}" style="margin-left:15px;">
<a href="">&nbsp;
<button class="btn btn-outline-dark" style="height:35px;"><i class="fas fa-search"></i></button>
</a>
</form>

<a href="{{url('addguru')}}"><button class="btn btn-outline-dark float-right" style="height:35px"><i class="fas fa-plus"></i> Tambah Data</button></a>

<table class="table shadow-sm">
    <thead style="background:#f4f4f4">     
        <tr>
            <th>Nama</th>
            <th>Mapel</th>
            <th>NIP</th>
            <th>Aksi</th>
        </tr>
    </thead>
    <tbody>
        <tr>
            <td>Zulfa</td>
            <td>Bindo</td>
            <td>002983</td>
            <td>
            <a href=""><button class="btn btn-outline-dark" style="height:35px;"><i class="fas fa-search"></i></button></a>
            <a href=""><button class="btn btn-outline-dark" style="height:35px;"><i class="fas fa-edit"></i></button></a>
            <a href=""><button class="btn btn-outline-dark" style="height:35px;"><i class="fas fa-trash"></i></button></a>
            </td>
        </tr>
    </tbody>
</table>
</div>

@stop