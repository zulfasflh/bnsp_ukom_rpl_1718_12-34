@extends('layouts.master')
@section('content')

<div class="container">
    <div class="jumbotron" style="background:transparent;color:#444;margin-top:-40px;">
        <h4 style="font-size:2.0em">
            INPUT DATA GURU
        </h4>
        <hr>
    </div>
</div>

<div class="container">
<div class="card">
    <div class="card-header text-center">
        <h5 class="display-5">Input Data Guru</h5>
    </div>
    <div class="card-body">
        <form action="{{route('createguru')}}" method="POST">
        @csrf
        <div class="row">
        <div class="md-form col-md-6">
        <label for="id_mapel">Mapel</label>
        <select name="id_mapel" id="" class="form-control" required>
            <option value="">Pilih...</option>
            <option value="1">1</option>
            <option value="2">2</option>
        </select>
        </div>

        <div class="md-form col-md-6">
        <label for="nama">Nama</label>
        <input type="text" placeholder="Nama" class="form-control" name="nama" required>            
        </div>

        <div class="md-form col-md-6 pt-3">
        <label for="username">Username</label>
        <input type="text" placeholder="Username" class="form-control" name="username" required>            
        </div>

        <div class="md-form col-md-6 pt-3">
        <label for="password">Password</label>
        <input type="password" placeholder="Password" class="form-control" name="password" required>            
        </div>

        <div class="md-form col-md-6 pt-3">
        <label for="nip">NIP</label>
        <input type="text" placeholder="Nip" class="form-control" name="nip" required>            
        </div>

        <div class="md-form col-md-6 pt-3">
        <label for="alamat">Alamat</label>
        <textarea class="form-control" name="alamat" id="alamat" cols="3" required></textarea>
        </div>

        <div class="md-form col-md-6">
        <label for="telpon">Telepon</label>
        <input type="number" placeholder="Telepon" class="form-control" name="telpon" required>            
        </div>

        <div class="md-form col-md-6 pt-3">
        <label for="gambar">Gambar</label>
        <input type="file" class="form-control-file" name="gambar">            
        </div>
        </div>

        <button type="submit" class="btn btn-outline-dark mt-3"> <i class="fas fa-plus"></i> Simpan</button>
        </form>
        <a href="{{URL::previous()}}">
        <button type="submit" class="btn btn-outline-dark"> <i class="fas fa-reply"></i> Kembali</button>       
        </a> 
    </div>
</div>  
</div>

@stop