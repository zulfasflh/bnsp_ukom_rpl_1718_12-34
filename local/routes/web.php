<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/guru', 'GuruAdmController@index')->name('guru');
Route::get('/addguru', 'GuruAdmController@add')->name('addguru');
Route::get('/guru/cari', 'GuruAdmController@cari')->name('guru/cari');
Route::post('/createguru', 'GuruAdmController@create')->name('createguru');
Route::get('/editguru', 'GuruAdmController@edit')->name('editguru');
Route::post('/updateguru', 'GuruAdmController@update')->name('updateguru');
Route::get('/detailguru', 'GuruAdmController@detail')->name('detailguru');
